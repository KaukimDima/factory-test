### create user
```
METHOD POST
http://localhost:8000/api/users/


login: str = usernameww
password: str = usernameww
```

```
{
    "code": 0,
    "data": {
        "login": "usernameww"
    }
}
```

### auth user

```
METHOD POST 
http://localhost:8000/api/auth/login/

login: str = usernameww
password: str = usernameww
```
```
{
    "code": 0,
    "data": {
        "token": "<ACCESS_TOKEN>",
        "user": {
            "login": "usernameww",
            "email": null
        }
    }
}
```


### user list

```
METHOD GET
http://localhost:8000/api/users/
-H "Authorization: Token <ACCESS_TOKEN>"
```
```
{
    "code": 0,
    "data": [
        {
            "login": "newUserik",
            "email": "admin@mail.kz"
        },
        ...
    ]
}
```


### Bot list

```
METHOD GET
http://localhost:8000/api/bots
-H "Authorization: Token <ACCESS_TOKEN>"
```
```
{
    "code": 0,
    "data": [
        {
            "id": 1,
            "name": "DFactoryDevBot",
            "token": "<TOKEN>"
        },
        ....
    ]
}
```


### Choose bot
```
METHOD POST
http://localhost:8000/api/bots
-H "Authorization: Token <ACCESS_TOKEN>"

bot_id: int 
```

```
REMOVE bot
METHOD DELETE
http://localhost:8000/api/bots
-H "Authorization: Token <ACCESS_TOKEN>"

bot_id: int 
```

После того, когда выбрали бота. 
Можно авторизоваться в телеграм, через бота, отправив <TOKEN>

[Telegram link]

```
METHOD GET
http://localhost:8000/api/messages/bots
-H "Authorization: Token <ACCESS_TOKEN>"

{
    "code": 0,
    "data": [
        {
            "id": 4,
            "tg_bot": 25,
            "chat_id": <CHAT_IDs>,
            "username": "userName",
            "is_active": true
        }
    ]
}
```

Выбрав нужный чат, монжо отправлять сообщения

```
METHOD POST 
http://localhost:8000/api/messages/send
-H "Authorization: Token <ACCESS_TOKEN>"

message: str = 'message1'
id: int = 4
```
```
{
    "code": 0,
    "data": {
        "id": 14,
        "message": "message1"
    }
}
```

### list all messages
```
METHOD GET
http://localhost:8000/api/messages/all
-H "Authorization: Token <ACCESS_TOKEN>"
```

[Telegram link]: <https://t.me/DFactoryDevBot>