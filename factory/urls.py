from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from factory import settings
from accounts.urls  import router as account_urls
# from api.urls import router as api_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(account_urls.urls)),
    path('api/', include('api.urls', namespace='main-api')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
