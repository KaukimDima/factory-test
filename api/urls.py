from rest_framework import routers
from api import views
from django.urls import path, include

app_name = 'api'

router = routers.DefaultRouter()
router.register(r'bots', views.BotViewSet, basename='bots')
router.register(r'messages', views.BotMessagesViewSet, basename='bots')

urlpatterns = [
    path('webhook/', views.TgWebhook.as_view()),
]

urlpatterns += router.urls