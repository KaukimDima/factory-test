import json
from django.utils.decorators import method_decorator
from django.db.models import Q, OuterRef, Subquery, Exists

from rest_framework import viewsets
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView

from api import models as api_models
from api import serializers
from api.serializers import BotCreateSerializer, BotSerializer, ChatBotMessageDateSerializer, ChatBotSerializer, ChatBotMessageSerializer

from utils.decorators import response_wrapper
from utils.base_permissions import IsOwner
from utils.telegram.base import TelegramActions

class TgWebhook(APIView):

    permission_classes = (AllowAny,)

    def get(self, request,):
        return Response({})

    def post(self, request,):
        bot_id = int(request.query_params.get('bot_id'))
        from_ = request.data['message']['from']

        text = request.data['message']['text']
        TelegramActions(from_=from_, bot_id=bot_id, text=text,)

        return Response({})

    def put(self, request,):
        print('put request.data:', request.data,)
        return Response({})

@method_decorator(response_wrapper(), name='dispatch')
class BotViewSet(viewsets.ModelViewSet):
    
    queryset = api_models.TgBot.objects.all()
    permission_classes = (IsAuthenticated,)
    http_method_names = ['get', 'post', 'delete',]
    serializer_class = BotSerializer

    def get_serializer_class(self):
        if self.action in ['delete', 'create']: 
            return BotCreateSerializer
        return super().get_serializer_class()

    def get_queryset(self):
        if self.action in ['delete', 'create']:
            return api_models.UsersTgBot.objects.all()
        return self.queryset.annotate(
            token=Subquery(
                api_models.UsersTgBot.objects.filter(
                    user=self.request.user,
                    bot__id=OuterRef('id')
                ).values('token')
            ),
        )

    def create(self, request,):
        serializer =  BotCreateSerializer(
            data=request.data, 
            context={'user_id': request.user.id,}, 
        )
        serializer.is_valid(raise_exception=True)
        payload = serializer.create(serializer.validated_data)
        return Response(payload)

    @action(methods=['delete'], detail=False, permission_classes=[IsOwner])
    def delete(self, request,):
        serializer = BotCreateSerializer(
            data=request.data,
            context={'user_id': request.user.id, 'bot_id': request.data.get('bot_id')}
        )
        payload = serializer.delete()
        return Response(payload)


@method_decorator(response_wrapper(), name='dispatch')
class BotMessagesViewSet(viewsets.GenericViewSet):
    
    queryset = api_models.UserTgChat.objects.all()
    permission_classes = (IsAuthenticated,)
    http_method_names = ['get', 'post', 'delete',]
    serializer_class = ChatBotSerializer

    @action(methods=['get'], detail=False,)
    def bots(self, request,):
        serializer = self.serializer_class(
            self.queryset.filter(tg_bot__user=self.request.user,),
            many=True
        )
        return Response(serializer.data)

    @action(methods=['post'], detail=False, )
    def send(self, request,):
        serializer = ChatBotMessageSerializer(
            data=request.data,
            context={'user': request.user,}, 
        )
        serializer.is_valid(raise_exception=True,)
        instance = serializer.create(serializer.validated_data)
        return Response(ChatBotMessageSerializer(instance).data)

    @action(methods=['get'], detail=False, )
    def all(self, request,):
        messages = (api_models.UserTgChatMessage.objects
            .filter(tg_chat__tg_bot__user=self.request.user)
            .order_by('date')
        )
        return Response(ChatBotMessageDateSerializer(messages, many=True,).data)
