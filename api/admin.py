from django.contrib import admin
from . import models

admin.site.register([
    models.TgBot,
    models.UsersTgBot,
    models.UserTgChat,
    models.UserTgChatMessage,
])