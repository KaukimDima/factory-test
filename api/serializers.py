from rest_framework import serializers
from accounts.models import User
from utils import validators, messages
from utils.exceptions import CommonException
from api import models as api_models
from utils.telegram.base import TelegramActions


class BotSerializer(serializers.Serializer):

    id = serializers.IntegerField()
    name = serializers.CharField(max_length=30, validators=[], )
    token = serializers.CharField()    


class BotCreateSerializer(serializers.Serializer):

    class Meta:
        model = api_models.UsersTgBot

    bot_id = serializers.IntegerField()

    def validate_bot_id(self, value):
        try:
            tg_bot = api_models.TgBot.objects.get(id=value,)
        except Exception as e: 
            raise CommonException(detail=messages.BOT_NOT_EXIST)
        bot_exist = api_models.UsersTgBot.objects.filter(
            bot=tg_bot,
            user__id=self.context['user_id']
        )
        if bot_exist.exists():
            raise CommonException(detail=messages.BOT_EXIST)

        return value

    def create(self, validated_data):
        user_id = self.context['user_id']
        bot_id = validated_data['bot_id']
        
        instance = api_models.UsersTgBot(
            bot_id=bot_id,
            user_id=user_id,
        )
        instance.save()
        return instance.to_json()

    def delete(self,):
        user_id = self.context['user_id']
        bot_id = self.context['bot_id']

        try:
            instance = api_models.UsersTgBot.objects.get(
                bot_id=bot_id,
                user_id=user_id,
            )
        except Exception as e:
            raise CommonException(detail=messages.BOT_NOT_EXIST)

        payload = instance.to_json()
        instance.delete() 

        return payload


class ChatBotSerializer(serializers.Serializer):

    id = serializers.IntegerField()
    tg_bot = serializers.PrimaryKeyRelatedField(
        queryset=api_models.UsersTgBot.objects.all()
    )

    chat_id = serializers.IntegerField()
    username = serializers.CharField()
    is_active = serializers.BooleanField()

class ChatBotMessageSerializer(serializers.Serializer):
    
    id = serializers.IntegerField()
    message = serializers.CharField()

    def validate_id(self, value):
        try:
            instance =  api_models.UserTgChat.objects.select_related('tg_bot').get(id=value)
        except Exception as e:
            raise CommonException(detail=messages.TELEGRAM_CHAT_NOT_FOUND)
        if self.context['user'] != instance.tg_bot.user:
            raise CommonException(detail=messages.TELEGRAM_CHAT_NOT_FOUND)
        return value

    def get_message(self, user_name, message,):
        return f"{user_name}, я получил от тебя сообщение:\n" +\
            f"{message}"  

    def create(self, validated_data,):
        tg_chat = api_models.UserTgChat.objects.get(id=validated_data['id'])
        message=validated_data['message']
        instance = api_models.UserTgChatMessage(
            tg_chat=tg_chat,
            message=message,
        )
        instance.save()
        schemas_data = {
            'id': tg_chat.chat_id, 
            'username': tg_chat.username,
        }
        bot_id = tg_chat.tg_bot.bot.id
        tg_actions = TelegramActions(
            from_=schemas_data,
            bot_id=bot_id,
            userChat=tg_chat
        )
        tg_actions.send_user_message(
            self.get_message(self.context['user'].login, message)
        )

        return instance


class ChatBotMessageDateSerializer(serializers.Serializer): 
    
    class Meta:
        model = api_models.UserTgChatMessage

    message = serializers.CharField()
    date = serializers.DateTimeField()