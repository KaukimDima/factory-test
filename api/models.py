from django.db import models
from accounts.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from utils.token import jwt_decode_handler, get_token  
import requests
from factory import settings

class TgBot(models.Model):
    
    name = models.CharField(max_length=100)
    bot_token = models.CharField(max_length=50)

    def __str__(self,):
        return self.name
    
    def save(self, *args, **kwargs,):
        super(TgBot, self).save(*args, **kwargs)
        r = requests.get(f'https://api.telegram.org/bot{self.bot_token}/setWebHook?url={settings.SITE_URL}api/webhook/?bot_id={self.id}')
        if r.status_code != 200:
            pass 

class UsersTgBot(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user',)
    bot = models.ForeignKey(TgBot, on_delete=models.CASCADE, related_name='user_bot',)
    token = models.TextField()

    def __str__(self):
        return f'{self.user} {self.bot}'

    @property
    def user_id(self): return self.user.id

    @property
    def bot_id(self): return self.bot.id
    
    def to_json(self):
        return {
            'user_id': self.user.id,
            'token': self.token,
            'bot_id': self.bot.id,
        }

@receiver(post_save, sender=UsersTgBot)
def _post_save_receiver(sender, instance, created, **kwargs):
    if created:
        token = get_token(instance.user, bot_id=instance.bot.id)
        instance.token = token 
        instance.save()

class UserTgChat(models.Model):

    tg_bot = models.ForeignKey(UsersTgBot, on_delete=models.CASCADE, related_name='tg_bot',)
    chat_id = models.IntegerField()
    username = models.CharField(max_length=100,)
    is_active = models.BooleanField(default=False,)


class UserTgChatMessage(models.Model):
    
    tg_chat = models.ForeignKey(UserTgChat, on_delete=models.CASCADE)
    message = models.TextField()
    date = models.DateTimeField(auto_now=True,)

    
    