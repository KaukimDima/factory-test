from rest_framework import routers
from accounts import views

app_name = 'accounts'

router = routers.DefaultRouter()

router.register(r'users', views.UsersViewSet,)
router.register(r'auth', views.AuthViewSet)
