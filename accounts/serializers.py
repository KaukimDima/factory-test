from rest_framework import serializers
from .models import User

from utils.exceptions import CommonException
from utils import messages, validators


class UserSerializer(serializers.HyperlinkedModelSerializer):
    
    class Meta:
        model = User
        fields = ('login', 'email',)

class UserRegisterSerializer(serializers.Serializer):
    
    class Meta:
        model = User
        fields = ('login', )

    login = serializers.CharField(max_length=30, validators=[validators.full_name_validator], )
    password = serializers.CharField(max_length=50, validators=[],)

    def validate_login(self, value):
        if User.objects.filter(login=value).exists():
            raise CommonException(detail=messages.USER_LOGIN_DUPLICATE)
        return value

    def create(self, validated_data):
        instance = User(**validated_data)
        instance.set_password(validated_data['password'])
        instance.save()
        return instance

    def to_representation(self, obj):
        rep = super(UserRegisterSerializer, self).to_representation(obj)
        rep.pop('password', None)
        return rep


class UserLoginSerializer(serializers.Serializer):

    class Meta:
        model = User
        fields = ('login', )

    login = serializers.CharField(max_length=30, validators=[validators.full_name_validator], )
    password = serializers.CharField(max_length=50, validators=[],)

    def validate(self, attrs):
        try:
            user = User.objects.get(login=attrs['login'])
        except Exception as e:
            raise CommonException(detail=messages.USER_DOESNT_EXIST)
        if not user.check_password(attrs['password']):
            raise CommonException(detail=messages.INVALID_PASSWORD)
        return attrs
    
    

    
            


        
