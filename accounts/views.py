from django.utils.decorators import method_decorator
from rest_framework import viewsets
from rest_framework import viewsets
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from accounts.models import User
from .serializers import (
    UserLoginSerializer, 
    UserRegisterSerializer, 
    UserSerializer
) 

from utils.decorators import response_wrapper


@method_decorator(response_wrapper(), name='dispatch')
class UsersViewSet(viewsets.ModelViewSet):

    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'post', 'put',]

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return None
        if not self.request.user.is_admin:
            return self.queryset.filter(id=self.request.user.id)
        return self.queryset

    def get_permissions(self):
        if self.action in ['list', 'update',]: 
            self.permission_classes = (IsAuthenticated,)
        else:
            self.permission_classes = (AllowAny,)
        return super().get_permissions()

    def get_serializer_class(self):
        if self.action in ['create', 'update',]:
            return UserRegisterSerializer
        return UserSerializer

    def update(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        instance = self.get_object()
        instance = serializer.update(instance, serializer.validated_data)
        return Response(UserRegisterSerializer(instance).data)


@method_decorator(response_wrapper(), name='dispatch')
class AuthViewSet(viewsets.GenericViewSet):

    queryset = User.objects.all()
    http_method_names = ['get', 'post', 'put',]

    def get_serializer_class(self):
        return UserLoginSerializer

    @permission_classes([AllowAny,])
    @action(methods=['post'], detail=False)
    def login(self, request):

        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        user = User.objects.get(login=serializer.validated_data['login'])
        token = Token.objects.get_or_create(user=user)

        return Response({
            'token': token[0].key,
            'user': UserSerializer(user).data,
        })
