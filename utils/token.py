from calendar import timegm
from datetime import datetime, timedelta
from rest_framework_jwt.settings import api_settings

jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

def jwt_payload_handler(user, **kwargs):
    username_field = 'login'
    login = user.login
    payload = {
        'user_id': user.pk,
        'login': login,
        'exp': datetime.utcnow() + timedelta(days=2)
    }

    payload[username_field] = login
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    for x, y in kwargs.items():
        payload[x] = y

    return payload


def get_token(user, **kwargs):
    payload = jwt_payload_handler(user, **kwargs)
    token = jwt_encode_handler(payload)
    return token
