import os
from datetime import datetime
from django.shortcuts import redirect

def default_datetime(): return datetime.now()

def upload_path(instance, filename):
	return os.path.join(str(instance.__class__.__name__).lower() + '/', filename)
