from datetime import datetime
from typing import List, Optional 
from pydantic import BaseModel, validator
import pytz

sf_timezone = pytz.timezone('Asia/Almaty')

class UserTgData(BaseModel):
    
    id: int 
    first_name: Optional[str] 
    last_name: Optional[str] 
    username: str

    bot_id: int
  
class TgMessage(BaseModel):

    date: datetime = None
    message: str

    @validator("date", pre=True)
    def date_valudate(cls, date,):
        return sf_timezone.localize(datetime.fromtimestamp(date))
