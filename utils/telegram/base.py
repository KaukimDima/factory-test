from typing import Optional, Union
import requests 
from utils.telegram.schemas import UserTgData
from accounts.models import User
from api import models as api_models
from utils.token import jwt_decode_handler
from utils import messages

class TelegramActions(object):

    user: User 
    userChat: api_models.UsersTgBot 
    bot: api_models.TgBot
    
    MESSAGE_ACTION = 'sendMessage'

    @classmethod
    def get_cls():
        pass 

    def __activate_user(self, bot_id: int, payload: dict,):
        user_id: int = payload['user_id'] 

        bot = api_models.UsersTgBot.objects.get(user__id=user_id, bot__id=bot_id)
        instance = api_models.UserTgChat( 
            tg_bot=bot,
            chat_id=self.user_tg_data.id,
            username=self.user_tg_data.username,
            is_active=True,
        )
        instance.save()
        self.send_user_message(f'{instance.username}! {messages.TELEGRAM_SUCCESS}')

    def __init__(self, *, from_: dict, bot_id: int, text: Optional[str] = None, userChat: Optional[api_models.UserTgChat] = None ): 
        self.user_tg_data = UserTgData(**{
            **from_,
            'bot_id': bot_id,
        })
        chat_id = self.user_tg_data.id

        if userChat:
            self.userChat = userChat 
        else:
            try:
                self.userChat = (api_models.UserTgChat.objects
                    .select_related('tg_bot__user', 'tg_bot__bot').get( 
                        chat_id=chat_id,
                        tg_bot__bot__id=bot_id
                    )
                )
            except Exception as e: 
                self.bot = api_models.TgBot.objects.get(id=bot_id)
                try:
                    payload = jwt_decode_handler(text)
                except Exception as e:
                    self.__error_message(messages.TELEGRAM_TOKEN_ERROR)
                    return 
                self.__activate_user(bot_id, payload)
                return 

        self.user = self.userChat.tg_bot.user
        self.bot = self.userChat.tg_bot.bot


    def get_bot_url(self, x):
        return f'https://api.telegram.org/bot{x}/'

    def send_user_message(self, message: str,):
        data = {
            'chat_id': self.user_tg_data.id,
            'text': message,
        }
        bot_url = f'{self.get_bot_url(self.bot.bot_token)}{self.MESSAGE_ACTION}'
        r = requests.post(bot_url, data)
        if r.status_code != 200:
            pass 

    def __error_message(self, erorr_message: str,):
        self.send_user_message(f'Ошиба: {erorr_message}')

# class TelegramResponse(TelegramActions):

#     def __init__(self, *, from_: dict, bot_id: int, date: int, text: str,):
#         super().__init__(from_=from_,)

#     def check_user(self):
#         pass 
    