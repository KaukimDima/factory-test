from django.core.validators import validate_email
import phonenumbers


def valid_phone(phone):
    try:
        phone_object = phonenumbers.parse(phone, None)
        if not phonenumbers.is_valid_number(phone_object):
            return False
        return True
    except:
        return False


def valid_email(email):
    try:
        validate_email(email)
        return True
    except:
        return False


def none_to_empty(v):
    if v:
        return v
    return ''
