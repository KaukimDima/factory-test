import re
from django.core.exceptions import ValidationError
from utils.messages import PHONE_INVALID, PASSWORD_INVALID, ASCII_LETTERS
from utils.string_utils import valid_phone
import string
from django.utils.translation import gettext as _

def phone_validator(phone):
    if not valid_phone(phone):
        raise ValidationError(PHONE_INVALID)

def password_validator(password):
    '''
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        8 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    '''

    # calculating the length
    length_error = len(password) < 8
    # searching for digits
    digit_error = re.search(r"\d", password) is None
    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None
    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None
    # overall result
    password_ok = not any([length_error, digit_error, uppercase_error,
                           lowercase_error])

    if not password_ok:
        raise (ValidationError(PASSWORD_INVALID))


def full_name_validator(v):
    for i in v:
        if i not in string.ascii_letters + '- ':
            raise ValidationError(ASCII_LETTERS)
