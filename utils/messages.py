from django.utils.translation import gettext_lazy as _

PHONE_INVALID = _('Неправильный формат телефона')
PASSWORD_INVALID = _('''
Пароль должен быть длиной от 8 символов,
должен содержать символы верхнего и нижнего регистра,
должен содержать цифры
''')
CODE_EXPIRED = _('Срок действия кода истек')
CODE_NOT_CORRECT = _('Неправильный код')
CODE_INACTIVE = _('Код активации недействительный')
MAX_ITERATION_EXCEED = _('Превышено кол-во попыток')
PERMISSION_DENIED = _('Доступ запрещен')
INVALID_STATUS = _('Неправильный статус')
INVALID_PASSWORD = _('Неправильный пароль')
USER_EXIST = _('Пользователь существует')
USER_LOGIN_DUPLICATE = _('Пользователь с таким логином уже есть')
USER_DOESNT_EXIST = _('Пользователь не существует')
BOT_NOT_EXIST = _('Бот не найден')
BOT_EXIST = _('Бот уже выбран')
PHONE_EXISTS = _('Данный номер телефона принадлежит другому пользователю')
INVALID_DATA = _('Неправильные данные в запросе')
EMAIL_EXISTS = _('Email занят другим пользователем')
INVALID_SIGNATURE = _('Неверная подпись запроса')
PURCHASE_DESCRIPTION = _('Покупка в приложении Level8')
SERVICE_UNAVAILABLE = _('Сервис временно недоступен')
ASCII_LETTERS = _('Ввод только на латинице')
TELEGRAM_TOKEN_ERROR = _('Вы не авторизованы, отправьте ТОКЕН выданный платформой')
TELEGRAM_NOTAUTH = _('Вы не авторизоваы')
TELEGRAM_SUCCESS = _('Вы были активированы')
TELEGRAM_HIDDEN = _('Бот не отвечает на запросы')
TELEGRAM_CHAT_NOT_FOUND = _('Чат не найден')
